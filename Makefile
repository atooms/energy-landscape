PROJECT = atooms/landscape
PACKAGE = atooms.landscape

.PHONY: version test install docs coverage pep8 clean

all: install

version:

test:
	python -m unittest discover -s tests

install: version
	python setup.py install

docs:
	rm -rf docs/api
	pdoc -o docs/api --force --html --skip-errors $(PROJECT)
	emacs --batch -l ~/.emacs.d/init.el ~/.emacs.d/config.el --file=docs/index.org -f org-rst-export-to-rst --kill
	orgnb.py docs/*.org
	make -C docs/ singlehtml

coverage:
	coverage run --source $(PACKAGE) -m unittest discover -s tests
	coverage report

pep8:
	autopep8 -r -i $(PROJECT)
	autopep8 -r -i tests
	flake8 $(PROJECT)

clean:
	find $(PROJECT) tests -name '*.pyc' -exec rm '{}' +
	find $(PROJECT) tests -name '__pycache__' -exec rm -r '{}' +
	rm -rf build/ dist/


