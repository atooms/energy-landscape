#!/usr/bin/env python

import unittest
import numpy
import scipy.optimize
from atooms.landscape.surfaces import potential_energy
from atooms.landscape.surfaces import muller_brown
import atooms.landscape as el


class Test(unittest.TestCase):

    def setUp(self):
        self.model_lj = {
            "potential": [
	        {
	            "type": "lennard_jones",
	            "parameters": {
		        "sigma": [[1.0]],
		        "epsilon": [[1.0]]
	            }
	        }
            ],	
            "cutoff": [
	        {
	            "type": "cut_shift",
	            "parameters": {"rcut": [[2.5]]}
	        }
            ]
        }
        self.model_ka = {
            "potential": [
                {
                    "type": "lennard_jones", 
                    "parameters": {
                        "sigma": [[1.0, 0.8], [0.8, 0.88]], 
                        "epsilon": [[1.0, 1.5], [1.5, 0.5]]
                    }
                }
            ], 
            "cutoff": [
                {
                    "type": "cut_shift", 
                    "parameters": {
                        "rcut": [[2.5, 2.0], [2.0, 2.2]]
                    }
                }
            ], 
            "metadata": {
                "name": "kob_andersen", 
                "doi": "10.1103/PhysRevLett.73.1376", 
                "reference": "Phys. Rev. Lett. 73, 1376 (1994)", 
                "notes": ""
            }
        }

    def test_molecule(self):
        # Backend specific
        from atooms.system import Particle, Cell, System
        from atooms.backends.f90 import Interaction

        s = System()
        s.particle = [Particle(species=1, position=[0., 0., 0.]),
                      Particle(species=1, position=[0.5, 1., -.1]),
                      Particle(species=1, position=[1.1, 0.1, 0.1])]
        s.cell = Cell([10., 10., 10.])
        s.interaction = Interaction(self.model_lj)
        res = el.conjugate_gradient(s)
        d = sum(s.particle[0].distance(s.particle[1])**2)**0.5
        self.assertAlmostEqual(d, 1.12246, places=5)

    # def test_api(self):
    #     from atooms.landscape import api
    #     api.pes('data/fcc.xyz', method='lbfgs', model='lennard_jones',
    #             file_out='/tmp/el.{method}')
    #     api.nma('data/fcc.xyz', model='lennard_jones',
    #             file_nma='/tmp/el.nma')
    #     # self.assertAlmostEqual(s.potential_energy(), -774.6, places=1)

    def _test_fcc(self, method, *args, **kwargs):
        from atooms.system import Particle, Cell, System
        from atooms.backends.f90 import Interaction
        from atooms.trajectory import Trajectory
        import copy
        with Trajectory('data/fcc.xyz') as th:
            s = th[0]
        numpy.random.seed(10)
        for p in s.particle:
            p.position += numpy.random.rand(3) * 0.2
        model = self.model_lj
        # TODO: this has a non local effect on other tests, if test_api if executed first, the results are different
        # model['cutoff'][0]['parameters']['rcut'] = s.cell.side[0] / 2
        s.species_layout = 'F'
        s.interaction = Interaction(model)        
        self.assertTrue(s.potential_energy() > -790.67)
        method(s, *args, **kwargs)
        self.assertAlmostEqual(s.potential_energy(), -790.67, places=1)
        return s

    def test_fcc_sd(self):
        self._test_fcc(el.steepest_descent)

    def test_fcc_cg(self):
        self._test_fcc(el.conjugate_gradient)

    def test_fcc_lbfgs(self):
        s = self._test_fcc(el.l_bfgs)
        db = el.normal_modes_analysis(s)
        self.assertEqual(db['number_of_unstable_modes'], 0)

    def test_fcc_wmin(self):
        self._test_fcc(el.force_minimization)

    def test_fcc_ef(self):
        s = self._test_fcc(el.eigenvector_following, unstable_modes=0, debug=True)
        db = el.normal_modes_analysis(s)
        self.assertEqual(db['number_of_unstable_modes'], 0)

    def test_fcc_update(self):
        import atooms.landscape.surfaces.potential_energy as pe
        # Reset update at the end
        pe.update = True
        s = self._test_fcc(el.eigenvector_following, unstable_modes=0, debug=True)
        db = el.normal_modes_analysis(s)
        self.assertEqual(db['number_of_unstable_modes'], 0)
        pe.update = False
        
    def test_fcc_ef_callback(self):
        def test(iteration, coords, *args):
            if iteration == 0:
                coords[0, 0] += 0.01

        s = self._test_fcc(el.eigenvector_following, unstable_modes=0, W_max=1e6, callback=test)
        db = el.normal_modes_analysis(s)
        self.assertEqual(db['number_of_unstable_modes'], 0)
        
    def test_fcc_fire(self):
        s = self._test_fcc(el.fire)

    def _test_muller_brown_ef(self, x0, y0, unstable_modes=-1,
                              freeze_iter=-1):
        from atooms.landscape.methods import eigenvector_following
        coords = numpy.array([x0, y0])
        eigenvector_following(coords,
                              muller_brown.compute,
                              muller_brown.normal_modes,
                              method='ef', max_iter=20000,
                              trust_radius=0.025,
                              trust_fixed=True,
                              unstable_modes=unstable_modes,
                              freeze_iter=freeze_iter,
                              dump_coords=True,
                              min_trust=1e-7, max_trust=10.0
        )
        return coords

    def _test_muller_brown_sd(self, x0, y0):
        from atooms.landscape.methods import steepest_descent
        coords = numpy.array([x0, y0])
        steepest_descent(coords,
                         muller_brown.compute,
                         dx=1e-5, maxiter=10000000)
        return coords

    def _test_muller_brown_fire(self, x0, y0):
        from atooms.landscape.methods import fire
        coords = numpy.array([x0, y0])
        fire(coords,
             muller_brown.compute)
        return coords
    
    def _test_muller_brown_cg(self, x0, y0):
        from scipy.optimize import minimize
        coords = numpy.array([x0, y0])
        res = minimize(muller_brown.value, coords, method='CG',
                          jac=muller_brown.gradient,
                          options={'gtol': 1e-10})
        return res['x']

    def _test_muller_brown_lbfgs(self, x0, y0):
        from scipy.optimize import minimize
        coords = numpy.array([x0, y0])
        result = minimize(muller_brown.value,
                          coords, method='L-BFGS-B',
                          jac=muller_brown.gradient,
                          options={'ftol': 1e-14,
                                   'gtol': 1e-10,
                                   'maxcor': 10})
        return result['x']
    
    def test_muller_brown(self):
        """See D. J. Wales, J. Chem. Phys. 101, 3750 (1994)."""
        min1, min2, min3 = (0.623499, 0.028038), (-0.558224, 1.441726), (-0.050011, 0.466694)
        ts1, ts2 = (-0.822021, 0.624313), (0.212487, 0.292988)
        points_dict = {
            # From Wales' paper
            (0.207484, 0.301647): ts2,  # 1
            (0.217490, 0.284330): ts2,  # 2
            (-0.5, 0.75): min3,  # 3
            (-0.6, 0.9): min2,  # 4
            (-0.814387, 0.6178299): ts1,  # 5
            (-0.05, -0.3): min1,  # 6
            (-0.5, -0.3): min3,  # 7
            (-1.3, 1.6): min2,  # 8
            (-0.829616, 0.630796): ts1,  # 9
            (0.9, 0.7): min1,  # 10
            (0.5, 1.7): min2,  # 11
            (0.8, 1.1): min3,  # 12
            (0.7, 1.3): min3,  # 13
            (0.6, 1.55): min2,  # 14
            (-1.0, -0.3): min3,  # 15
            (-1.3, -0.1): min2,  # 16
            # More points
            (-0.829, 0.630): ts1,
            (0.9, 0.7): min1,
            (0.5, 1.7): min2,
            # These should also converge
            (0.623, 0.028): min1,
            (-0.558, 1.441): min2,
            (-0.050, 0.466): min3,
            (-0.822, 0.624): ts1,
            (0.212, 0.292): ts2,
        }
        # # EF
        # for k in points_dict:
        #     if points_dict[k] in [min1, min2, min3]:
        #         nu = 0
        #     else:
        #         nu = 1
        #     coords = self._test_muller_brown_ef(k[0], k[1], unstable_modes=nu)
        #     ref_coords = points_dict[k]
        #     self.assertTrue(numpy.all(abs(ref_coords - coords) < 0.01))
        # # SD
        # for k in points_dict:
        #     if points_dict[k] in [min1, min2, min3]:
        #         coords = self._test_muller_brown_sd(k[0], k[1])
        #         ref_coords = points_dict[k]
        #         #print(numpy.all(abs(ref_coords - coords) < 0.01), coords, ref_coords)
        #         self.assertTrue(numpy.all(abs(ref_coords - coords) < 0.01))
        # # CG: avoid failures
        # for k in points_dict:
        #     if points_dict[k] in [min1, min2, min3] and \
        #        k not in [(-0.5, 0.75), (-0.5, -0.3), (0.6, 1.55)]:
        #         coords = self._test_muller_brown_cg(k[0], k[1])
        #         ref_coords = points_dict[k]
        #         #print(k, numpy.all(abs(ref_coords - coords) < 0.01), coords, ref_coords)
        #         self.assertTrue(numpy.all(abs(ref_coords - coords) < 0.01))
        # # LBFGS: avoid failures
        # for k in points_dict:
        #     if points_dict[k] in [min1, min2, min3] and \
        #        k not in [(-0.05, -0.3), (0.9, 0.7), (0.6, 1.55)]:
        #         try:
        #             coords = self._test_muller_brown_lbfgs(k[0], k[1])
        #         except OverflowError:
        #             print('FAILURE', k)
        #             continue
        #         ref_coords = points_dict[k]
        #         #print(k, numpy.all(abs(ref_coords - coords) < 0.01), coords, ref_coords)
        #         self.assertTrue(numpy.all(abs(ref_coords - coords) < 0.01))
        # FIRE: only one differs
        for k in points_dict:
            if points_dict[k] in [min1, min2, min3] and k != (-0.05, -0.3):
                coords = self._test_muller_brown_fire(k[0], k[1])
                ref_coords = points_dict[k]
                #print(k, numpy.all(abs(ref_coords - coords) < 0.01), coords, ref_coords)
                self.assertTrue(numpy.all(abs(ref_coords - coords) < 0.01))

    def test_ka_fire(self):
        import numpy
        from atooms.landscape import core
        from atooms import trajectory
        from atooms.backends import f90

        with trajectory.Trajectory('data/ka_N80.xyz') as th:
            s = th[0]
            s.interaction = f90.Interaction(self.model_ka)
            s.species_layout = 'F'
            res = core.fire(s)
            self.assertTrue(res['gradient_norm_square'] < 1e-10)

    def test_ka_sd_cm(self):
        import numpy
        from atooms.landscape import core
        from atooms import trajectory
        from atooms.backends import f90
        
        #with trajectory.Trajectory('data/ka_N80.xyz') as th:
        with trajectory.Trajectory('data/ka_N65.xyz') as th:
            idx = 20
            for fold in [True, False]:
                s = th[idx]
                s.interaction = f90.Interaction(self.model_ka)
                s.species_layout = 'F'
                res = core.steepest_descent(s, fold=fold)
                s_0 = th[idx]
                delta = s.dump('position', order='F') - s_0.dump('position', order='F')
                #print(numpy.max(delta))
            
if __name__ == '__main__':
    unittest.main(verbose=True)
