#!/usr/bin/env python

import unittest
import numpy


import numpy
from atooms.landscape.surfaces import muller_brown

class _SameMinimum(Exception):
    pass

def plot_mb():
    import numpy as np
    import matplotlib.pyplot as plt

    def create_mesh(f):
        from itertools import product
        x = np.arange(-1.5, 1, 0.025)
        y = np.arange(-0.5, 2, 0.025)
        X, Y = np.meshgrid(x, y)
        Z = np.zeros(X.shape)
        mesh_size = range(len(X))
        for i, j in product(mesh_size, mesh_size):
            x_coor = X[i][j]
            y_coor = Y[i][j]
            Z[i][j] = f(np.array([x_coor, y_coor]))
        return X, Y, Z

    def plot_contour(ax, X, Y, Z):
        CS = ax.contour(X, Y, Z, levels=np.linspace(-140, 100, 20))
        ax.axis('square')
        return ax

    from atooms.landscape.surfaces import muller_brown
    fig, ax = plt.subplots(figsize=(6, 6), frameon=False)
    X, Y, Z = create_mesh(muller_brown.value)
    ax = plot_contour(ax, X, Y, Z)
    return fig, ax


def _move_away_from_basin(s, s_next, tol=1e-7):
    from atooms.landscape import core
    # Coordinates must be unfolded
    pos = s.dump('particle.position', order='F', view=True)
    posn = s_next.dump('particle.position', order='F')
    pos_unf = s.dump('particle.position_unfolded', order='F')
    pos_next = s_next.dump('particle.position_unfolded', order='F')
    displ = pos_next - pos_unf
    
    pos_0 = pos.copy()
    res = core.steepest_descent(s, dx=1e-4, gtol=1e-10)
    #res = core.steepest_descent(s, dx=1e-4, gtol=1e-3)
    #res = core.fire(s, gtol=1e-10)
    u_0 = s.potential_energy()

    pos[...] = posn
    s.fold()
    res = core.steepest_descent(s, dx=1e-4, gtol=1e-10)
    #res = core.steepest_descent(s, dx=1e-4, gtol=1e-3)
    #res = core.fire(s, gtol=1e-10)
    u_n = s.potential_energy()

    # NOTE: the difference between these two minimizations may help setting the tolerance...?
    # pos[...] = pos_0 + displ
    # s.fold()
    # print(s.potential_energy(), 'u')
    # res = core.steepest_descent(s, dx=1e-4, gtol=1e-12)
    # #res = core.steepest_descent(s, dx=1e-4, gtol=1e-3)
    # #res = core.fire(s, gtol=1e-12)
    # print(res['function_along_path'][-5:])
    # u_nnn = s.potential_energy()
    
    # pos[...] = pos_0 + displ
    # print(s.potential_energy(), 'u')
    # res = core.steepest_descent(s, dx=1e-4, gtol=1e-12)
    # #res = core.steepest_descent(s, dx=1e-4, gtol=1e-3)
    # #res = core.fire(s, gtol=1e-12)
    # print(res['function_along_path'][-5:])
    # u_nn = s.potential_energy()

    # print(abs(u_0 - u_n) < tol, abs(u_0 - u_nn) < tol, abs(u_0 - u_nnn) < tol)
    # print(u_0)
    # print(u_n)
    # print(u_nn)
    # print(u_nnn)

    print(u_n, u_0, abs(u_0 - u_n) < tol)
    if abs(u_0 - u_n) < tol:
        raise _SameMinimum
    
    delta = 3e-1
    while delta < 2:
        delta *= 1.5
        pos[...] = pos_0 + delta * displ
        res = core.steepest_descent(s, dx=1e-4, gtol=1e-10)
        #res = core.steepest_descent(s, dx=1e-4, gtol=1e-3)
        #res = core.fire(s, gtol=1e-10)
        u = s.potential_energy()
        print(delta, u, u_0)
        if abs(u - u_0) > tol:
            print(delta, u, u_0, abs(u - u_0) > tol, 'away')
            pos = pos_0 + delta * displ  # this must be a copy, else it is a mess
            break

    pos_folded = s.dump('particle.position', view=True, order='F')
    pos_folded[...] = pos_0
    pos_next_folded = s_next.dump('particle.position', view=True, order='F')
    pos_next_folded[...] = pos

    return s_next


class Test(unittest.TestCase):

    def setUp(self):
        pass

    def _test_muller_brown(self, coords_0, coords_1, unstable_modes=-1,
                           freeze_iter=-1):
        from atooms.landscape.surfaces import muller_brown
        from atooms.landscape.methods import ridge
        db, db_side = [], []
        def store(iteration, coords, coords_side, *args):
            db.append(coords.copy())
            db_side.append(coords_side.copy())
        
        coords_0 = numpy.array(coords_0)
        coords_1 = numpy.array(coords_1)
        ridge(coords_0, 
              muller_brown.compute,
              muller_brown.normal_modes,
              coords_side=coords_1,
              callback=store, iter_sd=1, dt=1e-3, side_step=1e-1, iter_max=1000)

        # DEBUG
        # fig, ax = plot_mb()
        # xs = numpy.array(db)
        # ax.plot(xs[:, 0], xs[:, 1], linestyle='--', marker='o', color='orange', label='coords')
        # xs = numpy.array(db_side)
        # ax.plot(xs[:, 0], xs[:, 1], linestyle='--', marker='o', color='red', label='coords side')
        # fig.legend()
        # import matplotlib.pyplot as plt
        # plt.show()
        return coords_0

    def test_muller_brown(self):
        """See D. J. Wales, J. Chem. Phys. 101, 3750 (1994)."""
        #self.skipTest('ridge method still not working')
        min1, min2, min3 = (0.623499, 0.028038), (-0.558224, 1.441726), (-0.050011, 0.466694)
        ts1, ts2 = (-0.822021, 0.624313), (0.212487, 0.292988)
        coords_0 = (numpy.array(min3) + numpy.array(min2)) * 0.35
        coords_1 = (numpy.array(min3) + numpy.array(min2)) * 0.7
        coords = self._test_muller_brown(coords_0, coords_1)
        ref_coords = ts1
        self.assertTrue(numpy.all(abs(ref_coords - coords) < 0.01))
    
    def test_ka_ridge(self):
        self.skipTest('ridge method still not working')
        import numpy
        from atooms.landscape import core, fire
        from atooms.landscape.methods import ridge
        from atooms.landscape.surfaces import potential_energy
        from atooms import trajectory
        from atooms import models
        numpy.random.seed(10)
        potential_energy.update = True
        with trajectory.Trajectory('data/ka_N80.xyz') as th:
            s = th[0]
            s.interaction = models.f90.Interaction('kob_andersen')
            pos = s.dump('pos', view=True, order='F')
            pos_0 = pos.copy()
            res = fire(s)
            u_0 = s.potential_energy()
            # Get random direction
            displ = numpy.random.random(pos.shape) - 0.5
            delta = 1e-8
            while delta < 1:
                delta *= 2
                pos[...] = pos_0 + delta * displ
                res = fire(s)
                u = s.potential_energy()                
                if abs(u - u_0) / abs(u) > 1e-6:
                    pos = pos_0 + delta * displ  # this must be a copy, else it is a mess
                    break
        res = ridge(pos_0, potential_energy.value, potential_energy.gradient, potential_energy.normal_modes, coords_side=pos, args=(s, ), iter_max=50, iter_sd=5)
        #print(u_0, res['function'], u, res['gradient_norm_square']< 1e-10)
        self.assertTrue(res['gradient_norm_square']< 1e-10)
        self.assertTrue(u_0 < res['function'] > u)

    def test_ka_ridge_core(self):
        self.skipTest('ridge method still not working')
        import numpy
        from atooms.landscape import core
        from atooms import trajectory
        from atooms import models

        with trajectory.Trajectory('data/ka_N80_two.xyz') as th:
            s = th[0]
            s.interaction = models.f90.Interaction('kob_andersen')
            s_next = th[1]

            # Ridge method
            res_r = core.ridge(s, s_next)
            
            # Find minima
            s = th[0]
            s.interaction = models.f90.Interaction('kob_andersen')
            res_0 = core.fire(s)            
            s_next = th[1]
            s_next.interaction = models.f90.Interaction('kob_andersen')
            res_1 = core.fire(s_next)

            self.assertTrue(res_0['function'] <= res_r['function'] >= res_1['function'])
            self.assertTrue(res_r['gradient_norm_square'] < 1e-10)

    def test_ka_ridge_core_many(self):
        self.skipTest('ridge method still not working')
        import numpy
        from atooms.landscape import core
        from atooms import trajectory
        from atooms import models

        # Well, I am not sure there is a big difference wrt bare EF
        # If I make no bisection steps or a few, I get a slightly different saddle
        # but I have not checked the euclidian distance
        # One cross check would be finding the reaction path from the saddle and
        # check we find the same minima we started from.
        #with trajectory.Trajectory('data/ka_N80_two.xyz') as th:
        with trajectory.Trajectory('data/ka_N65.xyz') as th:
            #for i in range(0, len(th)-1, 100):
            #for i in range(3, len(th)):
            #for i in [22, 25]:
            #for i in range(len(th)-1):
            for i in [16]:  #range(16, 2, -1):
                s = th[i]
                s.interaction = models.f90.Interaction('kob_andersen')
                s_next = th[i+1]

                # Move in the direction of the next config, until we change basin
                try:
                    s_next = _move_away_from_basin(s, s_next)
                except _SameMinimum:
                    print('SKIP...', i)
                    continue

                try:
                    # This provides a good balance between bisections and descents, but it is still pretty slow and also goes to minima quite frequently
                    #res_r = core.ridge(s, s_next, iter_max=200, dt=1e-3, side_step=1e-3, iter_sd=20)
                    # We keep closer to the ridge: the bisection after 1 step gets immediately in 3 nearly identical minima
                    #res_r = core.ridge(s, s_next, iter_max=200, dt=1e-3, side_step=1e-4, iter_sd=20)
                    # We need a smaller step, else we get into identical minima: dt=1e-4 is now the same as the one used in _bisect()
                    res_r = core.ridge(s, s_next, iter_max=200, dt=1e-4, side_step=1e-4, iter_sd=40)
                    #res_r = core.eigenvector_following(s, trust_scale_down=1.1, trust_scale_up=1.1, trust_radius=0.01, unstable_modes=1, zero_mode=1e-6)
                    #print(res_r)
                    #continue
                except AssertionError as e:
                    print(e)
                    continue
                except ValueError as e:
                    print(e)
                    continue

                if res_r['number_of_unstable_modes'] != 1:
                    print('Not a TR! nu=', res_r['number_of_unstable_modes'], res_r['gradient_norm_square'], res_r['iterations'])
                    continue

                # Reaction path
                res = core.reaction_path(s)
                # Not quite the same...
                # TODO: monitors pairs of minima after bisection, they should be the same
                #print(res['u_0'], res['u_1'])
                # for _ in res['function_along_path']:
                #    print(_, ' # path')
                
                # Find minima
                s = th[i]
                s.interaction = models.f90.Interaction('kob_andersen')
                res_0 = core.fire(s)            
                s_next = th[i+1]
                s_next.interaction = models.f90.Interaction('kob_andersen')
                res_1 = core.fire(s_next)

                #print(res_r['success'], res_r['function'], res_r['gradient_norm_square'], res_r['number_of_unstable_modes'], res_r['iterations'])
                print(i, res_0['function'] <= res_r['function'] >= res_1['function'], res_r['success'], 'TS', res_r['function'],
                      'M0', min(res_0['function'], res_1['function']), min(res['u_0'], res['u_1']),
                      'M1', max(res_0['function'], res_1['function']), max(res['u_0'], res['u_1']),
                      res_r['gradient_norm_square'], res_r['number_of_unstable_modes'], res_r['iterations'])

                # # Find the TS that would be located by EF
                # s = th[i]
                # s.interaction = models.f90.Interaction('kob_andersen')
                
                # try:
                #     res_ef = core.eigenvector_following(s, unstable_modes=1)
                #     print(res_0['function'] <= res_r['function'] >= res_1['function'], res_ef['success'], res_ef['function'], res_ef['gradient_norm_square'], res_ef['number_of_unstable_modes'])
                # except:
                #     continue
                # print
            
if __name__ == '__main__':
    unittest.main(verbose=True)
