* Setup                                                    :noexport:ARCHIVE:
#+language: en
#+select_tags: export
#+exclude_tags: noexport
#+latex_class: article
#+latex_class_options: [11pt,a4paper]
#+latex_header: \usepackage[OT1]{fontenc}
#+latex_header: \linespread{1.1}
#+latex_header: \textwidth=16cm
#+latex_header: \textheight=25cm
#+latex_header: \hoffset=-1.8cm
#+latex_header: \voffset=-2.2cm
#+latex_header: \parindent=0ex
#+latex_header: \parskip=1ex
#+latex_header: \usepackage[scaled]{helvet}
#+latex_header: \renewcommand\familydefault{\sfdefault}
#+latex_header: \setlength{\leftmargini}{1em}
#+latex_header: \renewcommand{\labelitemi}{--}
#+latex_header: \usepackage{titling}
#+latex_header: \setlength{\droptitle}{-40pt}
#+latex_header: \pretitle{\begin{center}\bfseries\large}
#+latex_header: \predate{\begin{center}\vskip-40pt}
#+latex_header: \hypersetup{
#+latex_header:     colorlinks,%
#+latex_header:     citecolor=black,%
#+latex_header:     filecolor=black,%
#+latex_header:     linkcolor=blue,%
#+latex_header:     urlcolor=blue
#+latex_header: }
#+options: toc:2 num:nil title:nil author:nil email:nil timestamp:nil h:3 \n:nil @:t ::t |:t ^:{} _:{} *:t tex:t latex:t
#+html_head: <link rel="stylesheet" type="text/css" href="style.css" />
#+property: header-args:python :exports both
#+property: header-args:gnuplot :exports results

* Muller-Brown surface

#+begin_src python :session output :tangle mb.py
def ef(coords, trust_radius=0.2, trust_fixed=False,
       trust_dynamic=False, method='ef', freeze_iter=-1,
       freeze_unstable_modes=-1):
    import numpy
    from atooms.energy_landscape.surfaces import muller_brown
    from atooms.energy_landscape.methods import eigenvector_following

    if not trust_dynamic and not trust_fixed:
	trust_dynamic=True
    coords = numpy.array([float(_) for _ in coords.split(',')])
    eigenvector_following(coords,
			  function=muller_brown.value,
			  gradient=muller_brown.gradient,
			  normal_modes=muller_brown.normal_modes,
			  method=method,
			  maxiter=2000,
			  trust_radius=trust_radius,
			  trust_fixed=trust_fixed,
			  freeze_iter=freeze_iter,
                          freeze_unstable_modes=freeze_unstable_modes,
			  dump_coords=True,
			  max_trust=10.0,
			  min_trust=1e-7
    )

def sd(coords, dx=0.00001):
    import numpy
    from atooms.energy_landscape.backends import muller_brown
    from atooms.energy_landscape.sd import steepest_descent

    coords = numpy.array([float(_) for _ in coords.split(',')])
    steepest_descent(coords,
		     function=muller_brown.value,
		     gradient=muller_brown.gradient,
		     maxiter=2000,
		     dump_coords=True,
		     dx=dx
    )

if __name__ == '__main__':
    from argh import dispatch_commands
    dispatch_commands([ef, sd])
#+end_src

#+begin_src gnuplot :file mb.png
  p \
    '< echo  0.623 0.028' ti 'MI 1' pt 6 ps 2, \
    '< echo -0.558 1.441' ti 'M1 2' pt 6 ps 2, \
    '< echo -0.050 0.466' ti 'M1 3' pt 6 ps 2, \
    '< echo -0.822 0.624' ti 'TS 1' pt 6 ps 2, \
    '< echo  0.212 0.292' ti 'TS 2' pt 6 ps 2, \
    '< echo -0.829 0.630' ti '9'  pt 5 ps 1 lc 1, \
    '< echo 0.9 0.7'      ti '10' pt 7 ps 1 lc 1, \
    '< echo 0.5 1.7'      ti '11' pt 9 ps 1 lc 1, \
    '< cd ../;python docs/mb.py ef --freeze-unstable-modes 1 --trust-dynamic --trust-radius 0.025 " -0.829,0.630"' u 5:6 noti '9' w p pt 5 lc 1, \
    '< cd ../;python docs/mb.py ef --freeze-unstable-modes 0 --trust-dynamic --trust-radius 0.025 0.9,0.7'         u 5:6 noti '10' w p pt 7 lc 1, \
    '< cd ../;python docs/mb.py ef --freeze-unstable-modes 0 --trust-dynamic --trust-radius 0.025 " 0.5,1.7"'         u 5:6 noti '11' w p pt 9 lc 1, \
    '< cd ../;python docs/mb.py sd " -0.829,0.630"' u 5:6 noti '9' w l lc 1 dt 2, \
    '< cd ../;python docs/mb.py sd 0.9,0.7'         u 5:6 noti '10' w l lc 1 dt 2, \
    '< cd ../;python docs/mb.py sd 0.5,1.7'         u 5:6 noti '11' w l lc 1 dt 2
#+end_src

#+results:
[[file:mb.png]]

The problem is that when an eigenvalue gets close to 0 the system follows a path that keeps that mode to zero. These are the eigenvalues along such pathological path. Essentially the step oscillates back and forth around zero along this mode.
#+begin_src gnuplot :file /tmp/1.png
  #p '< cd ../;python docs/mb.py ef  --trust-fixed --trust-radius 0.025 --freeze-iter -1 0.9,0.7|grep aaa|tr -d []' u 0:7 w lp, '' u 0:8 w lp
  p '< cd ../;python docs/mb.py ef  --trust-fixed --trust-radius 0.025 --freeze-unstable-modes 0 0.5,1.7|grep aaa|tr -d []' u 0:7 w l, '' u 0:8 w lp
#+end_src

#+results:
[[file:/tmp/1.png]]

And here is the corresponding values of dx
#+begin_src gnuplot :file /tmp/2.png
  p '< cd ../;python docs/mb.py ef  --trust-fixed --trust-radius 0.025 0.9,0.7|grep aaa|tr -d []' u 0:1 w lp, '' u 0:2 w lp
#+end_src

#+results:
[[file:/tmp/2.png]]

So freezing at first step the signs appears necessary to avoid the step oscillating wildly between -1 and 1 when one eigenvalue crosses zero. The formula does not behave well in that case.

The dynamic trust radius also gives erratic behavior in the MB case, but it works well if I reduce the threshold error to 10% (instead of 100%)

Freezing the number of target unstable modes via an option (freeze_unstable_modes) instead of the actual value found at the first stp gives results consistent with the ones of Wales. It is the hybrid eigenvector-following / Newton-Rapshon thing that does not work well and gets stuck when the eigenvalue changes sign.

I also changed the code such that the zero modes are removed from the ousset. This way there is a more consistent mapping between eigenvalues across steps. Let's see what that gives from the bulk 3d.
